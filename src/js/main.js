// css読み込み
import '../scss/style.scss';

// jqueryでのDOM操作
import jQuery from 'jquery';
const $ = jQuery;
$(function fadeUpItem() {
  $(window).on('load', () => {
    $('.js-fadeup__item').css({
      'transition-delay': '.5s',
      'opacity': 1,
      'transform': 'translateY(0)'
    })
  })
})

$(function noScroll() {
  $('#indexPage').on('touchmove.noScroll', (e) => {
    e.preventDefault()
  })
})

// modernizr
import Modernizr from 'modernizr';
Modernizr.on('webp', (result) => {
  if (result) {
    $('.cover').addClass('webp');
  }
})

// lazysizes
require('lazysizes');
require('lazysizes/plugins/unveilhooks/ls.unveilhooks');


// superagentで非同期的にgetする
import superagent from 'superagent';
const request = superagent;
request
  .get('/about.html')
  .end((res) => {
    console.log('superagent :)')
  })
